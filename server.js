const express = require('express');
const app = express();
const port = process.env.PORT || 3000; // Porta padrão 3000, mas pode ser alterada

app.get('/', (req, res) => {
  res.send('Hello, World!'); // Rota de exemplo
});

app.listen(port, () => {
  console.log(`Servidor está rodando na porta ${port}`);
});
